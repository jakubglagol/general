# General

All discussions about the organization of the teams within the Configure stage.

Discussions related to Product development must happen in [gitlab](https://gitlab.com/gitlab-org/gitlab) issue trackers.

## Celebrating Our Successes

In Configure, we celebrate our successes. Here are a list of things we recently accomplished we are excited about.

| Name | Category | Issue | Date |
|------|----------|-------| -----|
| Terraform Plan in MR  | IaC  | https://gitlab.com/groups/gitlab-org/-/epics/2676  | 2020-04-17  |
| Managed Terraform Backend State  | IaC  | https://gitlab.com/groups/gitlab-org/-/epics/2673  | 2020-04-17  |
| Cloud Native Buildpacks  | K8s management  | https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28165  | 2020-04-13  |

## Outgoing MRs

As a part of [our Engineering Metrics](https://about.gitlab.com/handbook/engineering/management/engineering-metrics/#list-of-metrics),
we aim at measuring Merge Request rates both internally and externally. The list
below covers our **merged** outgoing MRs per milestone to community open source projects.
Additions to this list should be applied [using both `group` and `stage` labels
to properly track throughput](https://about.gitlab.com/handbook/engineering/management/throughput/#stage-and-group-labels-in-throughput).

### %13.3

- go-gitlab: [Add instance cluster API support](https://github.com/xanzy/go-gitlab/pull/893)
- go-gitlab: [Fix typos](https://github.com/xanzy/go-gitlab/pull/896)
- go-gitlab: [Add project template ID field to API](https://github.com/xanzy/go-gitlab/pull/904)

### %13.2

- gitops-engine: [refactor: improve func signature](https://github.com/argoproj/gitops-engine/pull/103)
- gitops-engine: [Handlers cleanups](https://github.com/argoproj/gitops-engine/pull/101)
- gitops-engine: [refactor: remove global semaphore](https://github.com/argoproj/gitops-engine/pull/100)
- gitops-engine: [fix: improve manifest parsing](https://github.com/argoproj/gitops-engine/pull/97)
- gitops-engine: [fix: data race on ctxCompleted](https://github.com/argoproj/gitops-engine/pull/86)
- gitops-engine: [fix: use streaming YAML decoder from Kubernetes](https://github.com/argoproj/gitops-engine/pull/85)
- gitops-engine: [refactor: Drop github.com/grpc-ecosystem/grpc-gateway dependency](https://github.com/argoproj/gitops-engine/pull/63)
- gitops-engine: [refactor: Replace ghodss/yaml with sigs.k8s.io/yaml](https://github.com/argoproj/gitops-engine/pull/62)
- gitops-engine: [refactor: use UpdateSettingsFunc type](https://github.com/argoproj/gitops-engine/pull/58)
- argoproj/pkg: [refactor: convert to Go modules](https://github.com/argoproj/pkg/pull/23)
- Kustomize: [Drop ghodss/yaml dependency](https://github.com/kubernetes-sigs/kustomize/pull/2630)
- Kubernetes: [Fix client config data race](https://github.com/kubernetes/kubernetes/pull/92139)
- Kubernetes cli-utils: [Use sigs.k8s.io/yaml for YAML](https://github.com/kubernetes-sigs/cli-utils/pull/195)

### %13.1

- gitops-engine: [refactor: reduce k8s.io/kubernetes usage](https://github.com/argoproj/gitops-engine/pull/57)
- gitops-engine: [fix: data race on err variable](https://github.com/argoproj/gitops-engine/pull/46)

### %13.0

- go-gitlab: [Fix token field data race](https://github.com/xanzy/go-gitlab/pull/828)
- terraform: [Fix typos](https://github.com/hashicorp/terraform/pull/24531)

### %12.10

- go-retryablehttp: [Add SetBody and WriteTo methods to Request](https://github.com/hashicorp/go-retryablehttp/pull/88)

### %12.8

- go-gitlab: [Add auto_ssl_enabled to pages domain api](https://github.com/xanzy/go-gitlab/pull/769)
